module.exports = {
  "env": {
    "browser": true,
    "es2021": true,
    "amd": true
  },
  "extends": "eslint:recommended",
  "parserOptions": {
    "ecmaVersion": 12
  },
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "always"
    ],
    "suitescript/script-type": "error",
    // "suitescript/no-log-module": "warn",
    "suitescript/no-invalid-modules": "error",
    "suitescript/api-version": "error",
    "suitescript/entry-points": "error",
    "suitescript/no-extra-modules": "error",
    "suitescript/log-args": "warn",
    "suitescript/module-vars": "error"
  },
  "plugins": ["suitescript"],
};