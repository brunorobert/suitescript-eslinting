This repository contains configuration for eslint for suitescript and some basic instructions on how to set things up.
The basic concept is have a npm project in a common root folder of all your suitescript projects. In this npm project
have an eslintrc file and install the suitescript plugin for eslint.

On can then either install eslint globally and lint ones suitescript files or use npx to run eslint.

Note that if you install the eslint plugin for VSCode, it will also usethe eslintrc file! yay!

# Requirements
- node > 12
- npm

# Initial Setup
1. cd to your home directory (or the root directory of all your suitescript projects) `cd ~/` or `cd ~/path/to/project/root`
2. initialize git repo in the directory: `git init`
3. add the remote `git remote add origin https://gitlab.com/brunorobert/suitescript-eslinting.git`
4. download the config files from the repository without merging `git pull origin master --ff-only`
5. install dependancies: `npm i`
6. (optional) install eslint globally: `npm i -g eslint eslint-plugin-suitescript`

# Linting your scrips

First cd to the directory containing your script. As long as your script is in a folder in one of the branches of the root folder you performed the setup in, the following should work.

`eslint script_name.js` (if you installed eslint globally)

or 

`npx eslint script_name.js` (if you chose not to install eslint globally)


